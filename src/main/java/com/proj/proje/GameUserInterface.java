package com.proj.proje;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface GameUserInterface extends MongoRepository<GameUser, String>{

	@Query("{'loginName':?0}")
	GameUser findByloginName(String name);

	
}
