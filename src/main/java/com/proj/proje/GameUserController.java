package com.proj.proje;

import java.io.Console;
import org.springframework.web.bind.annotation.RequestBody;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GameUserController {

	@Autowired
	GameUserInterface gameUserInterface;
	
	@RequestMapping(value="userRegistration", method=RequestMethod.POST)
	public boolean createGameUser(@RequestParam("username")String name, @RequestParam("password")String password) {
		if(gameUserInterface.findByloginName(name) == null) {
			GameUser gmusr = new GameUser(name, password, 0, new Date());
			gameUserInterface.insert(gmusr);
			return true;
		}
		else {
			return false;
		}

	}
	
	@RequestMapping(value="resetUser", method=RequestMethod.POST)
	public boolean resetUser(@RequestParam("username")String name) {
		GameUser userGot = gameUserInterface.findByloginName(name);
		gameUserInterface.delete(userGot);
		userGot = new GameUser(userGot.loginName, userGot.password, 0, new Date());
		gameUserInterface.save(userGot);
		return true;
	}
	
	@RequestMapping(value="userInit", method=RequestMethod.POST)
	public void initGameUser(@RequestParam("username")String name) {
		GameUser userGot = gameUserInterface.findByloginName(name);
		userGot.setLoggedIn(true);
		gameUserInterface.save(userGot);
	}
	@RequestMapping(value="updateUserTime", method=RequestMethod.POST)
	public void updateGameUserTime(@RequestParam("username")String name) {
		GameUser userGot = gameUserInterface.findByloginName(name);
		userGot.setLastLogin(new Date());
		gameUserInterface.save(userGot);
	}
	
	@RequestMapping(value="getUserResources", method=RequestMethod.POST)
	public double getUserResources(@RequestParam("username")String name) {
		GameUser userGot = gameUserInterface.findByloginName(name);
		return userGot.resourceInventory;
	}
	
	@RequestMapping(value="getUserResourcesWithTime", method=RequestMethod.POST)
	public double getUserResourcesWithTime(@RequestParam("username")String name) {
		GameUser userGot = gameUserInterface.findByloginName(name);
		double timeDifference = userGot.lastLogin.getTime() - new Date().getTime();
		double finalValue = timeDifference * userGot.resourceIncrease;
		return finalValue;
	}
	
	public double getUserResourcesWithTimeNoQuery(GameUser gmr) {
		double finalValue = 0;
		double timeDifference = (gmr.lastLogin.getTime() - new Date().getTime())/1000;
		if(((timeDifference * gmr.resourceIncrease) + gmr.resourceInventory) < Double.MAX_VALUE) {
			finalValue = ((timeDifference * gmr.resourceIncrease) + gmr.resourceInventory);
		}
		else {
			finalValue = Double.MAX_VALUE;
		}
		
		return finalValue;
	}
	
	@RequestMapping(value="getUserIncrease", method=RequestMethod.POST)
	public double getUserIncrease(@RequestParam("username")String name) {
		GameUser userGot = gameUserInterface.findByloginName(name);
		return userGot.resourceIncrease;
	}
	
	@RequestMapping(value="populateVariables", method=RequestMethod.POST)
	public double[] populateVariables(@RequestParam("username")String name) {
		GameUser userGot = gameUserInterface.findByloginName(name);
		double[] returnedValues = new double[9];
		returnedValues[0] = getUserResourcesWithTimeNoQuery(userGot);
		returnedValues[1] = userGot.resourceIncrease;
		returnedValues[2] = userGot.upgrades.get("pickaxe");
		returnedValues[3] = userGot.upgrades.get("drill");
		returnedValues[4] = userGot.upgrades.get("drone");
		returnedValues[5] = userGot.upgrades.get("laser");
		returnedValues[6] = userGot.upgrades.get("explosives");
		returnedValues[7] = userGot.upgrades.get("helpers");
		if(userGot.loggedIn) {
			returnedValues[8] = 1;
		}
		else {
			returnedValues[8] = 0;
			userGot.loggedIn = true;
			gameUserInterface.save(userGot);
		}
		
		return returnedValues;
	}
		
	@RequestMapping(value="saveUserResources", method=RequestMethod.POST)
	public void saveUserResources(@RequestParam("username")String name, @RequestParam("resources")double resources, @RequestParam("upgrades[]") String[] upgrades){
		GameUser userGot = gameUserInterface.findByloginName(name);
		userGot.resourceInventory = resources;
		userGot.lastLogin = new Date();
		Map<String, Integer> mappedValuesUpgrade = new HashMap<String, Integer>();
		for(String str: upgrades) {
			if(str.charAt(str.length()-2) == '1' && str.charAt(str.length()-1) == '0') {
				mappedValuesUpgrade.put(str.substring(0, str.length() - 2), Integer.parseInt(str.substring(str.length()-2, str.length())));
			}
			else {
				mappedValuesUpgrade.put(str.substring(0, str.length() - 1), Character.getNumericValue(str.charAt(str.length()-1)));
			}
			
		}
		userGot.setUpgrades((HashMap<String, Integer>) mappedValuesUpgrade);
		gameUserInterface.save(userGot);
	}
	
	@RequestMapping(value="getJSONConfigAsString", method=RequestMethod.POST)
	public String getJSONConfigAsString() {
		URL jsonFileURL = getClass().getResource("upgrades.json");
		File jsonFile = new File(jsonFileURL.getPath());
		String jsonString = null;
		try {
			jsonString = new String(Files.readAllBytes(jsonFile.toPath()), StandardCharsets.UTF_8);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonString;
	}
	
	@RequestMapping(value="userLogin", method=RequestMethod.POST)
	public Boolean GetUserFromDB(@RequestParam("username")String username, @RequestParam("password")String password) {
		GameUser gmr = gameUserInterface.findByloginName(username);
		System.out.println(gmr.loginName + " " + gmr.password + " | " + username + " " + password);
		if(gmr != null) {
			if(gmr.password.equals(password)) {
				System.out.println("User matched");
				return true;
			}
			else {
				System.out.println("Non-matching password");
				return false;
			}
		}
		else {
			System.out.println("User not found");
			return false;
		}
	}
	
}
