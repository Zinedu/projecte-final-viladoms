package com.proj.proje;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjecteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjecteApplication.class, args);
	}

}
