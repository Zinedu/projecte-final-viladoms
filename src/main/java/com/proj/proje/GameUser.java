package com.proj.proje;

import java.util.Date;

import java.util.HashMap;

import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Document;
@Document("GameUser")
public class GameUser {
	@Id
	String id;
	String loginName;
	String password;
	double resourceInventory;
	Date lastLogin;
	boolean loggedIn = false;
	double resourceIncrease = 0;
	HashMap<String, Integer> upgrades = new HashMap<String, Integer>();
	
	public GameUser() {
		this.upgrades.put("pickaxe", 0);
		this.upgrades.put("drill", 0);
		this.upgrades.put("drone", 0);
		this.upgrades.put("laser", 0);
		this.upgrades.put("explosives", 0);
		this.upgrades.put("helpers", 0);
	}
	
	public GameUser(String loginName, String password, double resourceInventory, Date lastLogin) {
		this.loginName = loginName;
		this.password = password;
		this.resourceInventory = resourceInventory;
		this.lastLogin = lastLogin;
		this.upgrades.put("pickaxe", 0);
		this.upgrades.put("drill", 0);
		this.upgrades.put("drone", 0);
		this.upgrades.put("laser", 0);
		this.upgrades.put("explosives", 0);
		this.upgrades.put("helpers", 0);
		
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getResourceInventory() {
		return resourceInventory;
	}

	public void setResourceInventory(double resourceInventory) {
		this.resourceInventory = resourceInventory;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public double getResourceIncrease() {
		return resourceIncrease;
	}

	public void setResourceIncrease(double resourceIncrease) {
		this.resourceIncrease = resourceIncrease;
	}

	public HashMap<String, Integer> getUpgrades() {
		return upgrades;
	}

	public void setUpgrades(HashMap<String, Integer> upgrades) {
		this.upgrades = upgrades;
	}
	
	
	
}
